import React from 'react';

const User = React.lazy(() => import('./views/base/User'));
const Transaction = React.lazy(() => import('./views/base/Transaction'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/user', name: 'User', component: User },
  { path: '/transaction', name: 'Transaction', component: Transaction },
];

export default routes;
