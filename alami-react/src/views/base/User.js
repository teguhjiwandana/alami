import React, {Component} from 'react'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CButton,
  CCardFooter
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from 'axios';
import Swal from 'sweetalert2';

const fields = ['name','birthDate', 'address',{
  key: 'show_details',
  label: '',
  _style: { width: '1%' },
  sorter: false,
  filter: false
}]

class User extends Component {

  constructor(props) {
    super(props);

    this.toggleDelete = this.toggleDelete.bind(this);
    this.toggleDetails = this.toggleDetails.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    users: [],
    modal: false,
    value: null,
    user: {
      name: '',
      address: '',
      birthDateString: ''
    },
    modalName: null,
    deleteId: null,
    show: false
  }

  handleShow = (param) => {
    let modalName = ""
    if (param != null) {
      modalName = "Edit"
    } else {
      modalName = "Add"
    }
    this.setState({
      modal: true,
      modalName: modalName
    })
    console.log(this.state)
  }
  handleClose = () => {
    this.setState({
      modal: false
    })
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleFilter(event) {
    const user = {page: 1, limit: 10, name: this.state.value}
    this.search(user)
    event.preventDefault();
  }

  handleSubmit(event) {
    axios.post(`http://localhost:8080/api/user/save`, this.state.user)
    .then(res => {
      if (res.data.errMsg != null) {
        console.log(res)
      } else {
        this.handleClose()
        const user = {page: 1, limit: 10}
        this.search(user)
      }
    })
    event.preventDefault();
  }

  componentDidMount() {
    const user = {page: 1, limit: 10}
    this.search(user)
  }

  search(param) {
    axios.post(`http://localhost:8080/api/user/search`, param)
    .then(res => {
      const users = res.data.content;
      this.setState({ users: users });
      console.log(res)
    })
  }

  handleInput(e, element) {
    const { user } = this.state;
    user[element] = e.target.value;
    this.setState({ user });
  }

  toggleDetails = (index) => {
    const user = index
    user.birthDateString = index.birthDate
    this.setState({user: user})
    console.log(this.state.user)
    
    this.handleShow("Edit")
  }

  toggleDelete = (index) => {
    const deleteId = index.id
    this.setState(
      {
        deleteId: deleteId,
        show: true
      }
    )

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.delete()
      }
    })
  }

  render() {
    return (
      <>
      <CRow>
        <CCol xs="12" sm="12">
          <form onSubmit={this.handleFilter}>
          <CCard>
            <CCardHeader>
              User Menu
              <div className="card-header-actions">
                <CButton 
                  color="primary"
                  onClick={this.handleShow} 
                  className="mr-1"
                >Add User</CButton>
              </div>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs="4">
                    <CFormGroup>
                      <CLabel htmlFor="name">Name</CLabel>
                      <CInput id="name" placeholder="Enter your name" onChange={this.handleChange} />
                    </CFormGroup>
                </CCol>
              </CRow>
            </CCardBody>
            <CCardFooter>
              <CButton type="submit" size="sm" color="primary"><CIcon name="cil-scrubber" /> Submit</CButton>
            </CCardFooter>
          </CCard>
          </form>
        </CCol>
        </CRow>
        <CRow>
          <CCol>
            <CCard>
              <CCardBody>
              <CDataTable
                items={this.state.users}
                fields={fields}
                hover
                striped
                bordered
                size="sm"
                itemsPerPage={10}
                pagination
                scopedSlots = {{
                  'show_details':
                    (item, index)=>{
                      return (
                        <td className="py-2">
                          <CButton
                            color="primary"
                            variant="outline"
                            shape="square"
                            size="sm"
                            onClick={() => this.toggleDetails(item)}
                          >
                            {'Show'}
                          </CButton>
                          <CButton
                            color="primary"
                            variant="outline"
                            shape="square"
                            size="sm"
                            onClick={() => this.toggleDelete(item)}
                          >
                            {'Delete'}
                          </CButton>
                        </td>
                        )
                    },
                }}
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        <CModal 
          show={this.state.modal} 
          onClose={this.handleClose}
        >
          <form onSubmit={this.handleSubmit}>
            <CModalHeader closeButton>
              <CModalTitle>{this.state.modalName} User</CModalTitle>
            </CModalHeader>
            <CModalBody>
              <CRow>
                <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="name">Name</CLabel>
                      <CInput id="name" placeholder="Enter your name" value={this.state.user.name} onChange={e => this.handleInput(e, 'name')} required />
                    </CFormGroup>
                </CCol>
                <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="name">Address</CLabel>
                      <CInput id="name" placeholder="Enter your address" value={this.state.user.address} onChange={e => this.handleInput(e, 'address')} required />
                    </CFormGroup>
                </CCol>
                <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="name">Birth Date</CLabel>
                      <CInput type="date" id="date-input" name="date-input" value={this.state.user.birthDate} placeholder="date" onChange={e => this.handleInput(e, 'birthDateString')} required />
                    </CFormGroup>
                </CCol>
              </CRow>
            </CModalBody>
            <CModalFooter>
              <CButton type="submit" color="primary">Save</CButton>{' '}
              <CButton 
                color="secondary" 
                onClick={this.handleClose}
              >Cancel</CButton>
            </CModalFooter>
          </form>
        </CModal>
      </>
    )
  }
}

export default User
