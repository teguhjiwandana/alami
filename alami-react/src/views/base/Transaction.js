import React, {Component} from 'react'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CButton,
  CCardFooter,
  CSelect
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from 'axios';
import Swal from 'sweetalert2';

const fields = ['transactionDate','user', 'transactionType', 'amount',{
  key: 'show_details',
  label: '',
  _style: { width: '1%' },
  sorter: false,
  filter: false
}]

class Transaction extends Component {

  constructor(props) {
    super(props);

    this.toggleDelete = this.toggleDelete.bind(this);
    this.toggleDetails = this.toggleDetails.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleInputFilter = this.handleInputFilter.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    users: [],
    transactions: [],
    modal: false,
    transactionFilter: {
      userId: null,
      startDateString: null,
      endDateString: null,
      page: 1,
      limit: 10
    },
    transaction: {
      id: null,
      userId: null,
      transactionDateString: null,
      amount: null,
      transactionType: null,
      user: null,
    },
    modalName: null,
    deleteId: null,
    show: false
  }

  handleShow = (param) => {
    let modalName = ""
    if (param != null) {
      modalName = "Edit"
    } else {
      modalName = "Add"
    }
    this.setState({
      modal: true,
      modalName: modalName
    })
    console.log(this.state)
  }
  handleClose = () => {
    this.setState({
      modal: false
    })
  }

  handleFilter(event) {
    this.searchTrx(this.state.transactionFilter)
    event.preventDefault();
  }

  handleSubmit(event) {
    axios.post(`http://localhost:8080/api/transaction/save`, this.state.transaction)
    .then(res => {
      if (res.data.errMsg != null) {
        Swal.fire(
          'Error!',
          res.data.errMsg,
          'error'
        )
      } else {
        Swal.fire(
          'Success!',
          'Your file has been saved.',
          'success'
        )
        this.handleClose()
        const transaction = {page: 1, limit: 10}
        this.searchTrx(transaction)
      }
    })
    event.preventDefault();
  }

  delete() {
    axios.get(`http://localhost:8080/api/transaction/delete/` + this.state.deleteId)
    .then(res => {
      if (res.data.errMsg != null) {
        Swal.fire(
          'Error!',
          res.data.errMsg,
          'error'
        )
      } else {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        this.handleClose()
        const transaction = {page: 1, limit: 10}
        this.searchTrx(transaction)
      }
    })
  }

  componentDidMount() {
    const user = {page: 1, limit: 10000}
    this.searchUser(user)

    const transaction = {page: 1, limit: 10}
    this.searchTrx(transaction)
  }

  searchUser(param) {
    axios.post(`http://localhost:8080/api/user/search`, param)
    .then(res => {
      const users = res.data.content;
      this.setState({ users: users });
      console.log(res)
    })
  }

  searchTrx(param) {
    if (param.userId == "") {
      param.userId = null
    }
    axios.post(`http://localhost:8080/api/transaction/search`, param)
    .then(res => {
      const transactions = res.data.content;
      this.setState({ transactions: transactions });
      console.log(res)
    })
  }

  handleInputFilter(e, element) {
    const { transactionFilter } = this.state;
    transactionFilter[element] = e.target.value;
    this.setState({ transactionFilter });
  }

  handleInput(e, element) {
    const { transaction } = this.state;
    transaction[element] = e.target.value;
    this.setState({ transaction });
  }

  toggleDetails = (index) => {
    const transaction = index
    if (index.user) {
      transaction.userId = index.user.id
    }
    transaction.transactionDateString = index.transactionDate
    this.setState({transaction: transaction})
    console.log(this.state.transaction)
    
    this.handleShow("Edit")
  }

  toggleDelete = (index) => {
    const deleteId = index.id
    this.setState(
      {
        deleteId: deleteId,
        show: true
      }
    )

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.delete()
      }
    })
  }

  render() {
    return (
      <>
      <CRow>
        <CCol xs="12" sm="12">
          <form onSubmit={this.handleFilter}>
          <CCard>
            <CCardHeader>
              Transaction Menu
              <div className="card-header-actions">
                <CButton 
                  color="primary"
                  onClick={this.handleShow} 
                  className="mr-1"
                >Add Transaction</CButton>
              </div>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs="4">
                    <CFormGroup>
                      <CLabel htmlFor="name">Start Date</CLabel>
                      <CInput type="date" id="date-input" name="date-input" placeholder="date" onChange={e => this.handleInputFilter(e, 'startDateString')} />
                    </CFormGroup>
                </CCol>
                <CCol xs="4">
                    <CFormGroup>
                      <CLabel htmlFor="name">End Date</CLabel>
                      <CInput type="date" id="date-input" name="date-input" placeholder="date" onChange={e => this.handleInputFilter(e, 'endDateString')} />
                    </CFormGroup>
                </CCol>
                <CCol xs="4">
                    <CFormGroup>
                      <CLabel htmlFor="name">User</CLabel>
                      <CSelect custom name="select" id="select" onChange={e => this.handleInputFilter(e, 'userId')}>
                        <option value="">-</option>
                        {this.state.users.map((option) => (
                            <option value={option.id}>{option.name}</option>
                        ))}
                      </CSelect>
                    </CFormGroup>
                </CCol>
              </CRow>
            </CCardBody>
            <CCardFooter>
              <CButton type="submit" size="sm" color="primary"><CIcon name="cil-scrubber" /> Submit</CButton>
            </CCardFooter>
          </CCard>
          </form>
        </CCol>
        </CRow>
        <CRow>
          <CCol>
            <CCard>
              <CCardBody>
              <CDataTable
                items={this.state.transactions}
                fields={fields}
                hover
                striped
                bordered
                size="sm"
                itemsPerPage={10}
                pagination
                scopedSlots = {{
                  'user':
                    (item)=>(
                      <td>
                        {item.user.name}
                      </td>
                    ),
                  'show_details':
                    (item, index)=>{
                      return (
                        <td className="py-2">
                          <CButton
                            color="primary"
                            variant="outline"
                            shape="square"
                            size="sm"
                            onClick={() => this.toggleDetails(item)}
                          >
                            {'Show'}
                          </CButton>
                          <CButton
                            color="primary"
                            variant="outline"
                            shape="square"
                            size="sm"
                            onClick={() => this.toggleDelete(item)}
                          >
                            {'Delete'}
                          </CButton>
                        </td>
                        )
                    },
          
  
                }}
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        <CModal 
          show={this.state.modal} 
          onClose={this.handleClose}
        >
          <form onSubmit={this.handleSubmit}>
            <CModalHeader closeButton>
              <CModalTitle>{this.state.modalName} Transaction</CModalTitle>
            </CModalHeader>
            <CModalBody>
              <CRow>
                <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="name">Transaction Date</CLabel>
                      <CInput type="date" id="date-input" name="date-input" value={this.state.transaction.transactionDate} placeholder="date" onChange={e => this.handleInput(e, 'transactionDateString')} required />
                    </CFormGroup>
                </CCol>
                <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="name">User</CLabel>
                      <CSelect custom name="select" id="select" value={this.state.transaction.userId} onChange={e => this.handleInput(e, 'userId')} required>
                        <option value="">-</option>
                        {this.state.users.map((option) => (
                            <option value={option.id}>{option.name}</option>
                        ))}
                      </CSelect>
                    </CFormGroup>
                </CCol>
                <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="name">Amount</CLabel>
                      <CInput type="number" placeholder="eg: 10000" value={this.state.transaction.amount} onChange={e => this.handleInput(e, 'amount')} required />
                    </CFormGroup>
                </CCol>
                <CCol xs="12">
                    <CFormGroup>
                      <CLabel htmlFor="name">Transaction Type</CLabel>
                      <CSelect custom name="select" id="select" value={this.state.transaction.transactionType} onChange={e => this.handleInput(e, 'transactionType')} required>
                        <option value="">-</option>
                        <option value="Menyerahkan">Menyerahkan</option>
                        <option value="Meminjam">Meminjam</option>
                      </CSelect>
                    </CFormGroup>
                </CCol>
              </CRow>
            </CModalBody>
            <CModalFooter>
              <CButton type="submit" color="primary">Save</CButton>{' '}
              <CButton 
                color="secondary" 
                onClick={this.handleClose}
              >Cancel</CButton>
            </CModalFooter>
          </form>
        </CModal>
      </>
    )
  }
}

export default Transaction
