import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'User',
    to: '/user',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Transaction',
    to: '/transaction',
  },
]

export default _nav
