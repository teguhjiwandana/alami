-- DROP SCHEMA public;

CREATE SCHEMA public AUTHORIZATION postgres;

COMMENT ON SCHEMA public IS 'standard public schema';


-- Drop table

-- DROP TABLE public.m_user;

CREATE TABLE public.m_user (
	id serial NOT NULL,
	"name" varchar NULL,
	birth_date date NULL,
	address varchar NULL,
	CONSTRAINT m_user_pkey PRIMARY KEY (id)
);


-- Drop table

-- DROP TABLE public."transaction";

CREATE TABLE public."transaction" (
	id serial NOT NULL,
	user_id int4 NULL,
	transaction_date date NULL,
	amount int4 NULL,
	transaction_type varchar NULL,
	CONSTRAINT transaction_pkey PRIMARY KEY (id)
);
