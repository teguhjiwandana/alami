package com.teguh.alamisharia.service;

import com.teguh.alamisharia.model.Transaction;
import com.teguh.alamisharia.model.dto.TransactionDto;
import com.teguh.alamisharia.repository.TransactionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class TransactionServiceTest {

    private TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);

    private TransactionService transactionService = Mockito.mock(TransactionService.class);

    @Test
    void saveTransaction() {
        when(transactionRepository.save(any(Transaction.class))).then(AdditionalAnswers.returnsFirstArg());
        TransactionDto dto = new TransactionDto();
        dto.setTransactionDateString("2021-01-01");
        dto.setUserId(1);
        dto.setAmount(1000);
        dto.setTransactionType("Menyerahkan");
        TransactionDto savedTransaction = transactionService.save(dto);
        assertThat(savedTransaction.getTransactionDate()).isNotNull();
    }

}