package com.teguh.alamisharia.service;

import com.teguh.alamisharia.model.User;
import com.teguh.alamisharia.model.dto.UserDto;
import com.teguh.alamisharia.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

class UserServiceTest {

    private UserRepository userRepository = Mockito.mock(UserRepository.class);

    private UserService userService = Mockito.mock(UserService.class);

    @Test
    void saveUser() {
        when(userRepository.save(any(User.class))).then(AdditionalAnswers.returnsFirstArg());
        UserDto dto = new UserDto();
        dto.setName("Testing User Name");
        dto.setAddress("Testing User Address");
        dto.setBirthDateString("2021-01-01");
        User savedUser = userService.save(dto);
        assertThat(savedUser.getName()).isNotNull();
    }
}