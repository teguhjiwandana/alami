package com.teguh.alamisharia.repository;

import com.teguh.alamisharia.model.User;
import com.teguh.alamisharia.model.dto.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "select t from User t where 1=1 " +
            "and (( t.name LIKE :#{#filter.name}) or ( null = :#{#filter.name}) ) " +
            "order by id desc")
    Page<User> findByFilter(@Param("filter") UserDto filter,
                                   Pageable pageable);
}
