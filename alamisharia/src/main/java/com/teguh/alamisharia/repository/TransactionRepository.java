package com.teguh.alamisharia.repository;

import com.teguh.alamisharia.model.Transaction;
import com.teguh.alamisharia.model.dto.TransactionDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    @Query(value = "select t from Transaction t where 1=1 " +
            "and ( (t.transactionDate >= :#{#filter.startDate} and t.transactionDate <= :#{#filter.endDate})  or ( null = :#{#filter.startDateString}) ) " +
            "and ( (t.user.id   = :#{#filter.userId})   or ( null = :#{#filter.userId}) ) " +
            "order by id desc")
    Page<Transaction> findByFilter(@Param("filter") TransactionDto filter,
                                Pageable pageable);
}
