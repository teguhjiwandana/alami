package com.teguh.alamisharia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class AlamishariaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlamishariaApplication.class, args);
    }

}
