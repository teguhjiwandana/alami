package com.teguh.alamisharia.controller;

import com.teguh.alamisharia.model.User;
import com.teguh.alamisharia.model.dto.UserDto;
import com.teguh.alamisharia.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value= "/save")
    public ResponseEntity<UserDto> save(@RequestBody UserDto dto ) {
        dto = userService.save(dto);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    @PostMapping(value= "/search")
    public ResponseEntity<User> search(@RequestBody UserDto dto ){
        Page<User> result = userService.findByFilter(dto);
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @GetMapping(value= "/delete/{id}")
    public ResponseEntity delete(@PathVariable Integer id ) {
        userService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
