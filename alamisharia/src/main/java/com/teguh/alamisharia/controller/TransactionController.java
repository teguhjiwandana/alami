package com.teguh.alamisharia.controller;

import com.teguh.alamisharia.model.Transaction;
import com.teguh.alamisharia.model.dto.TransactionDto;
import com.teguh.alamisharia.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping(value= "/save")
    public ResponseEntity<TransactionDto> save(@RequestBody TransactionDto dto ) {
        dto = transactionService.save(dto);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    @GetMapping(value= "/delete/{id}")
    public ResponseEntity delete(@PathVariable Integer id ) {
        transactionService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value= "/search")
    public ResponseEntity<Transaction> search(@RequestBody TransactionDto dto ){
        Page<Transaction> result = transactionService.findByFilter(dto);
        return new ResponseEntity(result, HttpStatus.OK);
    }
}
