package com.teguh.alamisharia.model.dto;

import com.teguh.alamisharia.model.Transaction;
import lombok.Data;

import java.time.LocalDate;

@Data
public class TransactionDto extends Transaction {

    private String startDateString;
    private String endDateString;
    private LocalDate startDate;
    private LocalDate endDate;
    private String transactionDateString;
    private Integer userId;
    private String errMsg;
    private Integer page;
    private Integer limit;
}
