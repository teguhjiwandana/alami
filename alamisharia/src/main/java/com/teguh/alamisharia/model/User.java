package com.teguh.alamisharia.model;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "m_user", schema = "public")
@Data
public class User implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_user_seq", sequenceName = "m_user_id_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_user_seq")
    private Integer id;

    @Column
    private String name;

    @Column
    private LocalDate birthDate;

    @Column
    private String address;
}
