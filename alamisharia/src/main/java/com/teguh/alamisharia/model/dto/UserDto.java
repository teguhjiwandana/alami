package com.teguh.alamisharia.model.dto;

import com.teguh.alamisharia.model.User;
import lombok.Data;

@Data
public class UserDto extends User {

    private String birthDateString;
    private String errMsg;
    private Integer page;
    private Integer limit;
}
