package com.teguh.alamisharia.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "transaction", schema = "public")
@Data
public class Transaction implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_transaction_seq", sequenceName = "transaction_id_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_transaction_seq")
    private Integer id;

    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column
    private LocalDate transactionDate;

    @Column
    private String transactionType;

    @Column
    private Integer amount;
}
