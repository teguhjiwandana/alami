package com.teguh.alamisharia.service;

import com.teguh.alamisharia.model.User;
import com.teguh.alamisharia.model.dto.UserDto;
import com.teguh.alamisharia.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserDto save(UserDto dto) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        User user = new User();
        if(dto.getId() != null) {
            user = userRepository.getOne(dto.getId());
        }
        try {
            user.setBirthDate(LocalDate.parse(dto.getBirthDateString(), formatter));
        } catch (Exception e) {
            e.getMessage();
        }
        user.setAddress(dto.getAddress());
        user.setName(dto.getName());
        userRepository.save(user);
        return dto;
    }

    public Page<User> findByFilter(UserDto dto) {
        PageRequest pageRequest = PageRequest.of (dto.getPage() -1, dto.getLimit(), Sort.by(Sort.Direction.DESC,"id"));
        dto.setName(dto.getName() == null ? null : "%" + dto.getName().trim() + "%");
        return userRepository.findByFilter(dto, pageRequest);
    }

    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

}
