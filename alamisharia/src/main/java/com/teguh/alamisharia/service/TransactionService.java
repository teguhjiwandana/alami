package com.teguh.alamisharia.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.teguh.alamisharia.model.Transaction;
import com.teguh.alamisharia.model.dto.TransactionDto;
import com.teguh.alamisharia.repository.TransactionRepository;
import com.teguh.alamisharia.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private KafkaTemplate<Object, Object> template;

    public TransactionDto save(TransactionDto dto) {

        validateRequest(dto);
        if(dto.getErrMsg() != null) {
            return dto;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Transaction transaction = new Transaction();
        if(dto.getId() != null) {
            transaction = transactionRepository.getOne(dto.getId());
        }
        transaction.setTransactionDate(LocalDate.parse(dto.getTransactionDateString(), formatter));
        transaction.setAmount(dto.getAmount());
        transaction.setTransactionType(dto.getTransactionType());
        transaction.setUser(userRepository.getOne(dto.getUserId()));
        transactionRepository.save(transaction);

        Gson gson = new Gson();
        this.template.send("alami-transaction", gson.toJson(dto));

        return dto;
    }

    public Page<Transaction> findByFilter(TransactionDto dto) {
        PageRequest pageRequest = PageRequest.of (dto.getPage() -1, dto.getLimit(), Sort.by(Sort.Direction.DESC,"id"));

        if(dto.getStartDateString() != null && dto.getEndDateString() != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            dto.setStartDate(LocalDate.parse(dto.getStartDateString(), formatter));
            dto.setEndDate(LocalDate.parse(dto.getEndDateString(), formatter));
        }
        return transactionRepository.findByFilter(dto, pageRequest);
    }

    public void delete(Integer id) {
        transactionRepository.deleteById(id);
    }

    private void validateRequest(TransactionDto dto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        if(LocalDate.now().compareTo(LocalDate.parse(dto.getTransactionDateString(), formatter)) <= 0) {
            dto.setErrMsg("Transaction Date cannot exceed today!!");
            return;
        }

        if(dto.getAmount() < 0) {
            dto.setErrMsg("Amount cannot minus!!");
            return;
        }
    }

    @KafkaListener(id = "kafka", topics = "alami-transaction")
    public void kafkakonsumer(String in) {
        System.out.println("Data yang ditarik : " + in);
    }
}
